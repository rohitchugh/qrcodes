from qrtools import QR
import os
'''
my_qr = QR(data = u"Quick Response Code!", pixel_size = 10)
my_qr.encode()
print my_qr.filename

os.system("sudo mv " + my_qr.filename + " ~/Dropbox/qrcodes/tmp")
'''

def encode(title, url):
    qr = QR(data = [title, url], data_type = 'bookmark')
    qr.encode()
    os.system("sudo mv " + qr.filename + " ~/Dropbox/qrcodes/tmp/" + title + ".png")

def decode(filepath):
    qr = QR(filename = filepath)
    qr.decode()

    print "Data: ", qr.data
    print "Data Type: ", qr.data_type
    print "Pixel Size: ", qr.pixel_size
    print "Margin Size: ", qr.margin_size
    print "LevelL ", qr.level

encode("google", "https://www.google.com/")
decode('/home/rohit/Dropbox/qrcodes/tmp/google.png')

